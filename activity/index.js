function Trainer(name, age, pokemon) {
	this.name = name;
	this.age = age;
	this.pokemon = pokemon;

	this.talk = function () {
		console.log('Pikachu I choose you!');
	};
}

const trainer = new Trainer('Ash', '12', [
	'Pikachu',
	'Charizard',
	'Greninja',
	'Sceptile',
	'Lucario',
	'Infernape',
]);

console.log(
	`Hi my name is ${trainer.name} and I'm ${trainer['age']} years old`
);
trainer.talk();

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	this.tackle = function (target) {
		console.log(`${this.name} tackled ${target.name}`);
		console.log(
			`${target.name} is now reduced to ${target.health - this.attack}`
		);

		target.health = target.health - this.attack;

		if (target.health <= 5) {
			target.faint();
		}
	};

	this.faint = function () {
		console.log(`${this.name} has fainted`);
	};
}

const charizard = new Pokemon('Charizard', 3);
const pikachu = new Pokemon('Pikachu', 6);

console.log(pikachu);
charizard.tackle(pikachu);
charizard.tackle(pikachu);
charizard.tackle(pikachu);
charizard.tackle(pikachu);
charizard.tackle(pikachu);
